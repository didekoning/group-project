Polymer Presentie informatiesysteem(PrIS)

Opdracht:
De opdracht voor jullie is om een Presentie informatiesysteem (PrIS) te ontwerpen, te realiseren en te testen dat het huidige werkwijze automatiseert, waardoor de hierboven beschreven processen efficiënter kunnen verlopen. Er zijn verschillende systeemonderdelen te ontdekken in de casus. Voor een voldoende moeten jullie zinnige en voldoende Use Cases bedenken die de belangrijke processen binnen de huidige werkwijze automatiseren. Use Cases zoals inloggen, uitloggen en wachtwoord wijzigen tellen niet; die kunnen wel nodig zijn maar tellen niet mee voor de beoordeling.

Eindrapport: 
https://docs.google.com/document/d/1Fg0Tm4AltcfssxB-PHRUzbY_NsGkVUSDiEIg3PFhIXE/edit?usp=sharing
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import model.Afwezigheid;
import model.Les;
import model.PrIS;
import model.Presentie;
import model.Student;
import model.Vak;
import model.PrIS;
import server.Conversation;
import server.Handler;

public class StudentController implements Handler {
	private PrIS informatieSysteem;
	
	/**
	 * De StudentController klasse moet alle student-gerelateerde aanvragen
	 * afhandelen. Methode handle() kijkt welke URI is opgevraagd en laat
	 * dan de juiste methode het werk doen. Je kunt voor elke nieuwe URI
	 * een nieuwe methode schrijven.
	 * 
	 * @param infoSys - het toegangspunt tot het domeinmodel
	 */
	public StudentController(PrIS infoSys) {
		informatieSysteem = infoSys;
	}

	public void handle(Conversation conversation) {
		if (conversation.getRequestedURI().startsWith("/student/afmelden")) {
			afmelden(conversation);
		}else if (conversation.getRequestedURI().startsWith("/student/mijnmedestudenten")) {
			mijnMedestudenten(conversation);
		}else if (conversation.getRequestedURI().startsWith("/student/betermelding")) {
			beterMelden(conversation);
		} else if(conversation.getRequestedURI().startsWith("/student/getLessen")){
			System.out.println("test");
			try {
				getLessen(conversation);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	else if (conversation.getRequestedURI().startsWith("/student/mijnAbsenties")){
			mijnAfwezigeheden(conversation);
		} 
	}

	/**
	 * Deze methode haalt eerst de opgestuurde JSON-data op. Daarna worden
	 * de benodigde gegevens uit het domeinmodel gehaald. Deze gegevens worden
	 * dan weer omgezet naar JSON en teruggestuurd naar de Polymer-GUI!
	 * 
	 * @param conversation - alle informatie over het request
	 */
	private void mijnMedestudenten(Conversation conversation) {
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		
		Student student = informatieSysteem.getStudent(gebruikersnaam);			// Student-object opzoeken
		String klasCode = student.getMijnKlas().getKlasCode();					// klascode van de student opzoeken
		ArrayList<Student> studentenVanKlas = informatieSysteem.getStudentenVanKlas(klasCode);	// medestudenten opzoeken
		
		JsonArrayBuilder jab = Json.createArrayBuilder();						// Uiteindelijk gaat er een array...
		
		for (Student s : studentenVanKlas) {									// met daarin voor elke medestudent een JSON-object... 
			if (s.getGebruikersNaam().equals(gebruikersnaam)) 					// behalve de student zelf...
				continue;
			else {
				jab.add(Json.createObjectBuilder()
						.add("naam", s.getGebruikersNaam()));
			}
		}
		
		conversation.sendJSONMessage(jab.build().toString());					// terug naar de Polymer-GUI!
	}
	
	private void mijnAfwezigeheden(Conversation conversation){
	
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		String vak = jsonObjectIn.getString("vak");

		Student student = informatieSysteem.getStudent(gebruikersnaam);

		ArrayList<Vak> vakken = student.getMijnKlas().getMijnVakken();
		
		JsonArrayBuilder jab = Json.createArrayBuilder();
		ArrayList<String> lijst = new ArrayList<String>();
		
		for(Vak v : vakken){
			if(!lijst.contains(v.getVakCode())){
				lijst.add(v.getVakCode());
			}	
		}
		for(String v: lijst){
			jab.add(Json.createObjectBuilder().add("vakcode", v));
		}
		if(!vak.equals("")){
			ArrayList<Afwezigheid> afwezigheid = student.getAbsentie();
			for(Afwezigheid a : afwezigheid){
				
				jab.add(Json.createObjectBuilder().add("afwezigheid",a.toString()));
			}
			
		}
		
		conversation.sendJSONMessage(jab.build().toString());
	}
	
	private static boolean isDateInCurrentWeek(Date date) {
		  Calendar currentCalendar = Calendar.getInstance();
		  int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
		  int year = currentCalendar.get(Calendar.YEAR);
		  Calendar targetCalendar = Calendar.getInstance();
		  targetCalendar.setTime(date);
		  int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
		  int targetYear = targetCalendar.get(Calendar.YEAR);
		  return week == targetWeek && year == targetYear;
	}
	
	private static Date asDate(LocalDate localDate) {
	    return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	private void getLessen(Conversation conversation) throws ClassNotFoundException, IOException{
		JsonObject jsonObjIn = (JsonObject) conversation.getRequestBodyAsJSON();
		JsonObjectBuilder job = Json.createObjectBuilder();
		String gebruikersnaam = jsonObjIn.getString("username");
		String wanneer = jsonObjIn.getString("wanneer");
		
		Student s = informatieSysteem.getStudent(gebruikersnaam);
		String klas = s.getMijnKlas().getKlasCode();
		
		ArrayList<ArrayList<String>> lessen = new ArrayList<ArrayList<String>>();
		File f = new File("lib/rooster_C.csv");
		Scanner sc = new Scanner(f);
		while(sc.hasNext()){
			String line = sc.nextLine();
			ArrayList<String> les = new ArrayList<String>();
			int prevI = -1;
			for(int i = 0; i < line.length() + 1 ; i++){
				try{
					if(String.valueOf(line.charAt(i)).equals(",")){
						les.add(line.substring(prevI + 1, i));
						prevI= i;
					}
				}
				catch(StringIndexOutOfBoundsException e){
					les.add(line.substring(prevI + 1, i));
				}
			}
			lessen.add(les);
		}
		sc.close();
		String rooster = "";
		if(wanneer.equals("vandaag")){
			for(ArrayList<String> les : lessen){
				if(les.get(0).equals(String.valueOf(LocalDate.now())) && les.get(6).equals(klas)){
					rooster += les + "\n";
				}
			}
		}
		
		if(wanneer.equals("week")){
			for(ArrayList<String> les: lessen){
				if(isDateInCurrentWeek(asDate(LocalDate.parse(les.get(0))))&& les.get(6).equals(klas)){
					rooster += les + "\n";
				}
			}
		}
		
		if(wanneer.equals("maand")){
			for(ArrayList<String> les: lessen){
				String datum  = les.get(0);
				String maand = datum.substring(datum.indexOf("-") + 1, datum.lastIndexOf("-"));
				if(Integer.parseInt(maand) == LocalDate.now().getMonthValue()&& les.get(6).equals(klas)){
					rooster += les + "\n";
				}
			}
		}
		System.out.println(rooster);
		job.add("lessen", rooster);
		conversation.sendJSONMessage(job.build().toString());
	}
	private void beterMelden(Conversation conversation) {
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		String button = jsonObjectIn.getString("button");
		Student student = informatieSysteem.getStudent(gebruikersnaam);			// Student-object opzoeken
		ArrayList<Afwezigheid> absentie = student.getAbsentie();
		int success = 0;
		if(button.equals("1")){	
			for(Afwezigheid a : absentie){
				if(a.getEindDatum() == null){
					Date dt = new Date();
					a.setEindDatum(dt);
					try {
						informatieSysteem.saveAfwezigheid();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					success = 1;
				}
			}
		}else{
			for(Afwezigheid a : absentie){
				if(a.getEindDatum() == null){
					success = 2;
				}
			}
		}

		JsonObjectBuilder job = Json.createObjectBuilder().add("success", success);			
		conversation.sendJSONMessage(job.build().toString());
		
	}
	
	private void afmelden(Conversation conversation) {
		JsonObject jsonObjIn = (JsonObject) conversation.getRequestBodyAsJSON();
		
		String gebruikersnaam = jsonObjIn.getString("username");
		String startDatum_temp = jsonObjIn.getString("startdate");			
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		 
		String eindDatum_temp = jsonObjIn.getString("enddate");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Date startDatum = null;
		Date eindDatum = null;
		int success = 0;
		String reden = jsonObjIn.getString("reason");
		System.out.println(eindDatum_temp);
		if(!eindDatum_temp.equals("onbekend")){
			try {
				startDatum = df.parse(startDatum_temp);
				eindDatum = df2.parse(eindDatum_temp);
				success = informatieSysteem.addAfwezigheid(startDatum, eindDatum, reden, gebruikersnaam);	
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				startDatum = df.parse(startDatum_temp);
				success = informatieSysteem.addAfwezigheid(startDatum, null, null, gebruikersnaam);	
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		JsonObjectBuilder job = Json.createObjectBuilder().add("success", success);			
		conversation.sendJSONMessage(job.build().toString());
	}
}

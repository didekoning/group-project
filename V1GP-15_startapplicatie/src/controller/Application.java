package controller;

import java.io.File;
import java.io.FileNotFoundException;

import model.PrIS;
import server.JSONFileServer;

public class Application {
	/**
	 * Deze klasse is het startpunt voor de applicatie. Hierin maak je een server 
	 * op een bepaalde poort (bijv. 8888). Daarna is er een PrIS-object gemaakt. Dit
	 * object fungeert als toegangspunt van het domeinmodel. Hiervandaan kan alle
	 * informatie bereikt worden.
	 * 
	 * Om het domeinmodel en de Polymer-GUI aan elkaar te koppelen zijn diverse controllers
	 * gemaakt. Er zijn meerdere controllers om het overzichtelijk te houden. Je mag zoveel
	 * controller-klassen maken als je nodig denkt te hebben. Elke controller krijgt een
	 * koppeling naar het PrIS-object om benodigde informatie op te halen.
	 * 
	 * Je moet wel elke URL die vanaf Polymer aangeroepen kan worden registreren! Dat is
	 * hieronder gedaan voor een drietal URLs. Je geeft daarbij ook aan welke controller
	 * de URL moet afhandelen.
	 * 
	 * Als je alle URLs hebt geregistreerd kun je de server starten en de applicatie in de
	 * browser opvragen! Zie ook de controller-klassen voor een voorbeeld!
	 * @throws FileNotFoundException 
	 * 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		JSONFileServer server = new JSONFileServer(new File("webapp/app"), 8888);
		System.out.println("nieuw");
		PrIS infoSysteem = new PrIS();
		
		UserController userController = new UserController(infoSysteem);
		DocentController docentController = new DocentController(infoSysteem);
		StudentController studentController = new StudentController(infoSysteem);
		
		server.registerHandler("/login", userController);
		server.registerHandler("/docent/mijnvakken", docentController);
		server.registerHandler("/docent/presentie", docentController);
		server.registerHandler("/docent/presentie-edit", docentController);
		server.registerHandler("/student/mijnmedestudenten", studentController);
		server.registerHandler("/student/afmelden", studentController);
		server.registerHandler("/student/betermelding", studentController);
		server.registerHandler("/student/getLessen", studentController);
		server.registerHandler("/student/mijnAbsenties", studentController);
		server.registerHandler("/docent/statistieken", docentController);
		server.registerHandler("/docent/rooster", docentController);
		server.registerHandler("/docent/studentpervak", docentController);
		server.registerHandler("/docent/presentieOpslaan", docentController);
		
		server.start();
	}
}
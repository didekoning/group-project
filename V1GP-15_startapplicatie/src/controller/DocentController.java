package controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import model.Docent;
import model.Les;
import model.PrIS;
import model.Presentie;
import model.Student;
import model.Vak;
import server.Conversation;
import server.Handler;

public class DocentController implements Handler {
	private PrIS informatieSysteem;
	
	/**
	 * De DocentController klasse moet alle docent-gerelateerde aanvragen
	 * afhandelen. Methode handle() kijkt welke URI is opgevraagd en laat
	 * dan de juiste methode het werk doen. Je kunt voor elke nieuwe URI
	 * een nieuwe methode schrijven.
	 * 
	 * @param infoSys - het toegangspunt tot het domeinmodel
	 */
	public DocentController(PrIS infoSys) {
		informatieSysteem = infoSys;
	}
	
	public void handle(Conversation conversation) {
		if (conversation.getRequestedURI().startsWith("/docent/mijnvakken")) {
			mijnVakken(conversation);
		}else if(conversation.getRequestedURI().startsWith("/docent/presentieOpslaan")){
			try {
				presentieOpslaan(conversation);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(conversation.getRequestedURI().startsWith("/docent/presentie")){
			presentie(conversation);
		}else if(conversation.getRequestedURI().startsWith("/docent/presentie-edit")){
			presentieEdit(conversation);
		} else if(conversation.getRequestedURI().startsWith("/docent/statistieken")){
			getPresentiePerDag(conversation);
		}else if(conversation.getRequestedURI().startsWith("/docent/rooster")){
			rooster(conversation);
		} else if(conversation.getRequestedURI().startsWith("/docent/studentpervak")){
			studentenVolgenVak(conversation);
		}
	}
	
	/**
	 * 
	 * Deze methode haalt eerst de opgestuurde JSON-data op. Daarna worden
	 * de benodigde gegevens uit het domeinmodel gehaald. Deze gegevens worden
	 * dan weer omgezet naar JSON en teruggestuurd naar de Polymer-GUI!
	 * 
	 * @param conversation - alle informatie over het request
	 * @throws ParseException 
	 */
	private void presentie(Conversation conversation) {
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		String edit = jsonObjectIn.getString("edit");
		if(edit.equals("false")){
			ArrayList<Student> studenten = informatieSysteem.getStudentenVanLes(gebruikersnaam);
			TreeMap<String, Boolean> totaalStudentenPerDag = new TreeMap<String, Boolean>();

			JsonArrayBuilder jab = Json.createArrayBuilder();

			for(Student s : studenten){
				jab.add(Json.createObjectBuilder()							
						.add("student", s.getGebruikersNaam())
						.add("present", informatieSysteem.getHuidigeAanwezigheid(s)));
			}
			conversation.sendJSONMessage(jab.build().toString());
		}else{
			System.out.println(gebruikersnaam+"test");
		}
		

	}
	
	private void presentieOpslaan(Conversation conversation) throws IOException{
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		JsonArray presentieLijst = jsonObjectIn.getJsonArray("presentielijst");
		Les currentLes = informatieSysteem.getCurrentLes();
		ArrayList<Student> studenten = informatieSysteem.getDeStudenten();
		ArrayList<String> presenties = new ArrayList<String>();
		for(int i =0; i <presentieLijst.size(); i++){
			JsonObject jo = presentieLijst.getJsonObject(i);
			JsonValue aanwezig = jo.get("value");
			JsonValue n = jo.get("key");
			String naam = n.toString().replace("\"", "");
			Student student = informatieSysteem.getStudent(naam);
			if(student != null){
				for(Vak vak : student.getMijnKlas().getMijnVakken()){
					for(Les les: vak.getLessen()){
						if(les.getDatum().equals(currentLes.getDatum())){
							student.addPresentie(currentLes, Boolean.valueOf(aanwezig.toString()));
							presenties.add(student.getGebruikersNaam() +"," + student.getPresentie(currentLes).toString());
						}
					}
				}
			}
		}
		informatieSysteem.savePresenties(presenties);
	}
	
	private ArrayList<String> getVakkenVanDocent(String naam){
		Docent docent = informatieSysteem.getDocent(naam);	// Docent-object ophalen!
		ArrayList<Les> lessen = docent.getLessen();						// Vakken van de docent ophalen!
		ArrayList<String> vakken = new ArrayList<String>();
		for (Les l : lessen) {
			if(!vakken.contains(l.getVakNaam())){
				vakken.add(l.getVakNaam());
			}
		}
		return vakken;
	}
	
	private void mijnVakken(Conversation conversation) {
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		JsonArrayBuilder jab = Json.createArrayBuilder(); 				// En uiteindelijk gaat er een JSON-array met...
		for(String s: getVakkenVanDocent(gebruikersnaam)){
			jab.add(Json.createObjectBuilder()							// daarin voor elk vak een JSON-object...
					.add("vaknaam", s));
		}
		conversation.sendJSONMessage(jab.build().toString());			// terug naar de Polymer-GUI!
	}
	private void studentenVolgenVak(Conversation conversation){
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String vak = jsonObjectIn.getString("vak");
		ArrayList<Les> lessen = informatieSysteem.getDocent(jsonObjectIn.getString("username")).getLessen();
		Map<String, ArrayList<String>> studentenPerVak = new HashMap<String, ArrayList<String>>();
		for(Student s: informatieSysteem.getDeStudenten()){
			for(Vak v : s.getMijnKlas().getMijnVakken()){
				for(Les l: v.getLessen()){
					for(Les les: lessen){
						ArrayList<String> stu = new ArrayList<String>();
						if(l.getDatum().equals(les.getDatum()) && l.getVakNaam().equals(les.getVakNaam()) && v.getVakCode().equals(vak)){//als student zelfde les als docent heeft,
							if(studentenPerVak.containsKey(l.getVakNaam())){
								stu = studentenPerVak.get(l.getVakNaam());
								if(!stu.contains(s.getGebruikersNaam())){
									stu.add(s.getGebruikersNaam());
									studentenPerVak.put(l.getVakNaam(), stu);
								}
							} else{
								stu.add(s.getGebruikersNaam());
								studentenPerVak.put(l.getVakNaam(), stu);
							}
						}
					}
				}
			}
		}
		
		JsonArrayBuilder jab = Json.createArrayBuilder();
		for(Map.Entry<String, ArrayList<String>> entry: studentenPerVak.entrySet()){
			for(String s: entry.getValue()){
				jab.add(Json.createObjectBuilder()							// daarin voor elk vak een JSON-object...
						.add(entry.getKey(), s));
			}
			
		}
		conversation.sendJSONMessage(jab.build().toString());
	}
	private boolean isDateInCurrentWeek(Date date) {
		  Calendar currentCalendar = Calendar.getInstance();
		  int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
		  int year = currentCalendar.get(Calendar.YEAR);
		  Calendar targetCalendar = Calendar.getInstance();
		  targetCalendar.setTime(date);
		  int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
		  int targetYear = targetCalendar.get(Calendar.YEAR);
		  return week == targetWeek && year == targetYear;
	}
	
	private void rooster(Conversation conversation){
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String gebruikersnaam = jsonObjectIn.getString("username");
		String wanneer = jsonObjectIn.getString("wanneer");
		Docent d = informatieSysteem.getDocent(gebruikersnaam);
		ArrayList<Les> lessen = d.getLessen();
		String huidigeDatum = new Date().toLocaleString();//dd-mmm-jjj hh:mm:ss
		String rooster = "";
		
		if(wanneer.equals("vandaag")){
			for(Les les : lessen){
				String datumLes = les.getDatum().toLocaleString().substring(0, les.getDatum().toLocaleString().indexOf(" "));
				String huidigetijd = huidigeDatum.substring(0, huidigeDatum.indexOf(" "));
				if(datumLes.equals(huidigetijd)){
					rooster += les.getVakNaam() + ", " + les.getDatum().toLocaleString().substring(0, les.getDatum().toLocaleString().indexOf(" ")) + " , " + les.getStartTijd() + " , " + les.getEindTijd() + "\n";
				}
			}
		}
		
		else if(wanneer.equals("week")){
			for(Les les: lessen){
				if(isDateInCurrentWeek(les.getDatum())){
					rooster += les.getVakNaam() + " , " + les.getDatum().toLocaleString().substring(0, les.getDatum().toLocaleString().indexOf(" ")) + " , " + les.getStartTijd() + " , " + les.getEindTijd() + "\n";
				}
			}
		}
		
		else if(wanneer.equals("maand")){
			for(Les les: lessen){
				String datum  = les.getDatum().toLocaleString();
				String maand = datum.substring(datum.indexOf("-") + 1, datum.lastIndexOf("-"));
				String huidigeMaand= huidigeDatum.substring(huidigeDatum.indexOf("-") + 1, huidigeDatum.lastIndexOf("-"));
				if(maand.equals(huidigeMaand)){
					rooster += les.getVakNaam() + " , " + les.getDatum().toLocaleString().substring(0, les.getDatum().toLocaleString().indexOf(" ")) + " , " + les.getStartTijd() + ", " + les.getEindTijd() + "\n";
				}
			}
		}
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("lessen", rooster);
		conversation.sendJSONMessage(job.build().toString());
	}
	private void presentieEdit(Conversation conversation){
		
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		String student = jsonObjectIn.getString("student");
		System.out.println(student+"hoi");
	}
	
	private void getPresentiePerDag(Conversation conversation){
		JsonObject jsonObjectIn = (JsonObject) conversation.getRequestBodyAsJSON();
		//String gebruikersnaam = jsonObjectIn.getString("username");
		//Docent docent = informatieSysteem.getDocent(gebruikersnaam);
		ArrayList<Student> studenten = informatieSysteem.getDeStudenten();
		ArrayList<Presentie> presenties = new ArrayList<>();
		TreeMap<String, Integer> totaalStudentenPerDag = new TreeMap<String, Integer>();
		TreeMap<String, Double> presentiePerDag = new TreeMap<String, Double>();
		String presentiePerDagS = "";
		Format format = new SimpleDateFormat("MM-dd-yyyy");
		for(Student s: studenten){
			for(Presentie p: s.getPresenties()){
				presenties.add(p);
			}
		}
		for(Presentie p: presenties){
			long ld = p.getLes().getDatum().toInstant().toEpochMilli();
			Date d = new Date(ld);
			//String datum = d.toLocaleString().substring(0, d.toLocaleString().indexOf(" "));
			Date date = new Date(ld);
		    String datum = format.format(date);
			//String datum = p.getLes().getDatum().toLocaleString().substring(0, p.getLes().getDatum().toLocaleString().indexOf(" "));
			if(totaalStudentenPerDag.containsKey(datum)){
				int value = totaalStudentenPerDag.get(datum);
				totaalStudentenPerDag.put(datum, value + 1);
			} else{
				totaalStudentenPerDag.put(datum, 1);
			}
		}
		for (Entry<String, Integer> entry : totaalStudentenPerDag.entrySet())
		{
			String datum = entry.getKey();
			int aantalStudenten = entry.getValue();
			double present = 0;
			//TreeMap<String, Integer> presentPerDag = new TreeMap<String, Integer>();
			for(Presentie p: presenties){
				//System.out.println(datum + " " + p.getLes().getDatum().toLocaleString().substring(0, p.getLes().getDatum().toLocaleString().indexOf(" ")));
				if(p.getAanwezigheid() && format.format(p.getLes().getDatum().toInstant().toEpochMilli()).equals(datum)){
					present += 1;
				}
			}
			//System.out.println("aantalstudenten: " + aantalStudenten + " aan present: " + present);
			presentiePerDag.put(entry.getKey(), present/aantalStudenten);	    
		}
		for (Entry<String, Double> entry : presentiePerDag.entrySet()){
			String datum = entry.getKey();
			presentiePerDagS += datum + ":" + entry.getValue() + "\n";
		}
		
		//presentiePerDagS.substring(0, presentiePerDagS.length() - 2);
		presentiePerDagS = presentiePerDagS.substring(0, presentiePerDagS.length() - 1);
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("presentie", presentiePerDagS);
		conversation.sendJSONMessage(job.build().toString());
	}
}

package model;

import java.util.ArrayList;

public class Docent extends Persoon{
	private ArrayList<Les> mijnLessen = new ArrayList<Les>();
	
	
	public Docent(String gebruikersNaam, String wachtwoord) {
		super(gebruikersNaam, wachtwoord);
	}
	
	public void voegLesToe(Les nwL) {
		if(!mijnLessen.contains(nwL)){
			mijnLessen.add(nwL);
		}
	}
	
	public ArrayList<Les> getLessen() {
		return mijnLessen;
	}
}
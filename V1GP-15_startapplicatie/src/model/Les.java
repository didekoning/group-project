package model;

import java.util.Date;

public class Les {
	private String startTijd;
	private String eindTijd;
	private Date datum;
	private String lokaal;
	private String vakNaam;
	
	public Les(String startTijd, String eindTijd, int weekNummer, Date datum, String lokaal, String vakNaam){
		this.startTijd = startTijd;
		this.eindTijd = eindTijd;
		this.datum = datum;
		this.lokaal = lokaal;
		this.vakNaam = vakNaam;
	}
	
	public String getVakNaam(){
		return vakNaam;
	}
	
	public String getStartTijd(){
		return this.startTijd;
	}
	public String getEindTijd(){
		return this.eindTijd;
	}
	public Date getDatum(){
		return this.datum;
	}
	
	
	
	public String toString(){
		String s = new String(vakNaam + "," + datum);
		return s;
	}
	
}

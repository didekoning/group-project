package model;

import java.util.Date;


public class Afwezigheid {
	private Date startDatum;
	private Date eindDatum;
	private String reden;
	
	public Afwezigheid(Date startDatum){
		this(startDatum, null, "ziekte");
	}
	
	public Afwezigheid(Date startDatum, Date eindDatum, String reden){
		this.startDatum = startDatum;
		this.eindDatum = eindDatum;
		this.reden = reden;
	}
	
	public Date getEindDatum(){
		return this.eindDatum;
	}
	public Date getStartDatum(){
		return this.startDatum;
	}
	
	public void setEindDatum(Date eindDatum){
		if(this.eindDatum == null){
			this.eindDatum = eindDatum;
		}
	}
	public boolean equals(Object obj){
		boolean gelijkeObjecten = false; 
		
		if (obj instanceof Afwezigheid) {
			Afwezigheid andereObject = (Afwezigheid) obj;
			 
			 if (this.startDatum.equals(andereObject.startDatum) &&
					 this.eindDatum == (andereObject.eindDatum)) {
			 gelijkeObjecten = true;
			 }
		}
		return gelijkeObjecten;
	}
	public String getDates(){
		return this.startDatum + "," + this.eindDatum;
	}
	public String toString(){
		return this.startDatum + "," + this.eindDatum + "," + this.reden;
	}
	
}

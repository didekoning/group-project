package model;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import javax.json.Json;
import javax.json.JsonArrayBuilder;

public class PrIS {
	private ArrayList<Docent> deDocenten;
	private ArrayList<Student> deStudenten;
	private Les current;
	
	/**
	 * De constructor maakt een set met standaard-data aan. Deze data
	 * moet nog vervangen worden door gegevens die uit een bestand worden
	 * ingelezen, maar dat is geen onderdeel van deze demo-applicatie!
	 * 
	 * De klasse PrIS (PresentieInformatieSysteem) heeft nu een meervoudige
	 * associatie met de klassen Docent en Student. Uiteraard kan dit nog veel
	 * verder uitgebreid en aangepast worden! 
	 * 
	 * De klasse fungeert min of meer als ingangspunt voor het domeinmodel. Op
	 * dit moment zijn de volgende methoden aanroepbaar:
	 * 
	 * String login(String gebruikersnaam, String wachtwoord)
	 * Docent getDocent(String gebruikersnaam)
	 * Student getStudent(String gebruikersnaam)
	 * ArrayList<Student> getStudentenVanKlas(String klasCode)
	 * 
	 * Methode login geeft de rol van de gebruiker die probeert in te loggen,
	 * dat kan 'student', 'docent' of 'undefined' zijn! Die informatie kan gebruikt 
	 * worden om in de Polymer-GUI te bepalen wat het volgende scherm is dat getoond 
	 * moet worden.
	 * 
	 */
	public PrIS() {
		deDocenten = new ArrayList<Docent>();
		deStudenten = new ArrayList<Student>();
		try {
			laadKlas();
			laadRooster();
			laadAfwezigheid();
//			for(int i = 0; i < deDocenten.size(); i++){
//				System.out.println(deDocenten.get(i).getGebruikersNaam()+" : "+deDocenten.get(i).getVakken());
//			}
			/*for(int i = 0; i < deStudenten.size(); i++){
				ArrayList<Vak> vakken = deStudenten.get(i).getMijnKlas().getMijnVakken();
				for(int a = 0; a < vakken.size(); a++){
					ArrayList<Les> lessen = vakken.get(a).getLessen();
					for(int b = 0; b < lessen.size(); b++){
						System.out.println(lessen.get(b).getStartTijd());
					}
				}
			}*/
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Docent d1 = new Docent("Wim", "geheim");
		Docent d2 = new Docent("Hans", "geheim");
		Docent d3 = new Docent("Jan", "geheim");
		
		/*d1.voegVakToe(new Vak("TCIF-V1AUI-15", "Analyse en User Interfaces"));
		d1.voegVakToe(new Vak("TICT-V1GP-15", "Group Project"));
		d1.voegVakToe(new Vak("TICT-V1OODC-15", "Object Oriented Design & Construction"));*/
		
		deDocenten.add(d1);
		deDocenten.add(d2);
		deDocenten.add(d3);
		
		Student s1 = new Student(12, "Roel", "Roel", "geheim");
		Student s2 = new Student(13, "Frans", "Roel", "geheim");
		Student s3 = new Student(14, "Daphne", "Roel", "geheim");
		Student s4 = new Student(15, "Jeroen", "Jeroen", "geheim");
		
		Klas k1 = new Klas("SIE-V1X");
		
		s1.setMijnKlas(k1);
		s2.setMijnKlas(k1);
		s3.setMijnKlas(k1);
		s4.setMijnKlas(k1);
		
		deStudenten.add(s1);
		deStudenten.add(s2);
		deStudenten.add(s3);
		deStudenten.add(s4);
	}
	//Laad de klas file en maak alle studenten aan in het geheugen
	public void laadKlas() throws IOException{
		File f = new File("lib/klassen.csv");
		
		if (f.exists() && f.isFile() && f.canRead()) {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String regel = br.readLine();
			while(regel != null){
				String[] values = regel.split(",");
				int studentNummer = Integer.parseInt(values[0]);
				String naam, gebruikersnaam = null;
				if(values[2] != ""){
					naam = values[3] + " " + values[2] + " " + values[1];
					gebruikersnaam = values[3] + "."  + values[2] + values[1];
				}else{
					naam = values[3] + " " + values[1];
				}
				String klasCode = values[4];
				Student x = new Student(studentNummer, gebruikersnaam.toLowerCase(),  naam, "geheim");
				if(klasCode != null){
					Klas klas = new Klas(klasCode);
					x.setMijnKlas(klas);
				}
				deStudenten.add(x);
				regel = br.readLine();
			}
			br.close();
		}
	}
	
//	public void laadPresenties() throws ParseException, IOException, ArrayIndexOutOfBoundsException{
//		File f = new File("lib/presentie.csv");
//		if(f.exists() && f.isFile() && f.canRead()){
//			FileReader fr = new FileReader(f);
//			BufferedReader br = new BufferedReader(fr);
//			String regel = br.readLine();
//			while(regel != null){
//				String[] vars = regel.split(",");
//				String studentnaam = vars[0];
//				String vakcode = vars[1];
//				String lesdatum = vars[2];
//				String aanwezigheid = vars[3];
//				try{
//					DateFormat df = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
//					Date datum = df.parse(lesdatum);
//				} catch(ParseException e){
//					e.printStackTrace();
//				}
//				for(Student s: deStudenten){
//					//System.out.println("s.gebruikersnaam: " + s.getGebruikersNaam() + " studentnaam: ");
//					if(s.getGebruikersNaam().equals(studentnaam)){
//						for(Vak vak: s.getMijnKlas().getMijnVakken()){
//							for(Les les: vak.getLessen()){
//								if(les.getDatum().equals(lesdatum)){
//									s.addPresentie(les, Boolean.parseBoolean(aanwezigheid));
//									System.out.println(s.getGebruikersNaam() + " " + les + " " + Boolean.parseBoolean(aanwezigheid));
//								}
//							}
//						}
//					}
//				}
//				regel = br.readLine();
//			}
//			br.close();
//		}
//
//	}
	
	public void laadAfwezigheid() throws IOException, ParseException{
		File f = new File("lib/afwezigheid.csv");
		
		if (f.exists() && f.isFile() && f.canRead()) {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String regel = br.readLine();
			while(regel != null){
				String[] values = regel.split(",");
				String startDatum_temp = values[0];
				String eindDatum_temp = values[1];
				String reden = values[2];
				String student = values[3];
				DateFormat df = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
				for(Student s : deStudenten){
					if(eindDatum_temp.equals("null")){
						if(s.getGebruikersNaam().equals(student)){
							Date startDatum = df.parse(startDatum_temp);
							s.addAbsentie(startDatum);
						}
					}else{
						if(s.getGebruikersNaam().equals(student)){
							Date startDatum = df.parse(startDatum_temp);
							Date eindDatum = df.parse(eindDatum_temp);
							s.addAbsentie(startDatum, eindDatum, reden);
						}
					}
				}
				regel = br.readLine();
			}
			br.close();
		}
	}
	
	//Laad de vakken file en maak alle studenten aan in het geheugen
	public void laadRooster() throws IOException, ParseException{
		File f = new File("lib/rooster_C.csv");
		
		if (f.exists() && f.isFile() && f.canRead()) {
			FileReader fr1 = new FileReader(f);
			BufferedReader br1 = new BufferedReader(fr1);
			String regel = br1.readLine();
			while(regel != null){
				String[] values = regel.split(",");
				String datum_temp = values[0];
			    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			    Date datum =  df.parse(datum_temp);  
				String startTijd = values[1];
				String eindTijd = values[2];
				String lesCode = values[3];
				String docent = values[4].toLowerCase();
				String lokaal = values[5];
				String klasCode = values[6];
				Vak v = new Vak(lesCode, lesCode);
				Les l = new Les(startTijd, eindTijd, 0, datum, lokaal, v.getVakNaam());
				Docent d = new Docent(docent, "geheim");
				d.voegLesToe(l);
				if(!deDocenten.contains(d)){
					deDocenten.add(d);
				}
				v.addLes(l);
				for(Student s : deStudenten  ){
					if(s.getMijnKlas().getKlasCode().equals(klasCode)){
						if(!s.getMijnKlas().getMijnVakken().contains(v)){
							s.getMijnKlas().voegVakToe(v);
							s.addPresentie(l, true);
						}
					}
				}
				for(Docent d1 : deDocenten){
					if(d1.gebruikersNaam.equals(docent)){
						d1.voegLesToe(l);
					}
				}
				
				
				regel = br1.readLine();
			}
			br1.close();
			}else{
				System.out.println("Bestand niet gevonden");
			}
		
	}
	public boolean getHuidigeAanwezigheid(Student s){
		ArrayList<Afwezigheid> afwezigheid = s.getAbsentie();
		Date now = new Date();

		for(Afwezigheid a : afwezigheid){
			if(a.getStartDatum().before(now) && a.getEindDatum().after(now)){
				return false;
			}
			 
		}
		return true;
	}
	
	public int addAfwezigheid(Date startDatum, Date eindDatum, String reden, String gebruikersnaam){
		for(Student s : deStudenten){
			Boolean result = false;
			if(s.getGebruikersNaam().equals(gebruikersnaam)){
				if(eindDatum == null && reden == null){
					result = s.addAbsentie(startDatum);
				}else{
					result = s.addAbsentie(startDatum, eindDatum, reden);
				}
				try {
					saveAfwezigheid();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(result == false){
					return 2;
				}
				return 3;
			}
		}
		return 1;
	}
	public void saveAfwezigheid() throws IOException{
		checkFile("lib/afwezigheid.csv");
		File f = new File("lib/afwezigheid.csv");
		FileWriter fw = new FileWriter(f);
		PrintWriter pw = new PrintWriter(fw);
		for(Student s : deStudenten){
			ArrayList<Afwezigheid> afwezigheid = s.getAbsentie();
			for(Afwezigheid a : afwezigheid){
				pw.println(a.toString()+","+s.getGebruikersNaam());
			}
		}
		pw.close();
	}
	
	public void savePresenties(ArrayList<String> presenties) throws IOException{
		checkFile("lib/presentie.csv");
		File f = new File("lib/presentie.csv");
		FileWriter fw = new FileWriter(f);
		PrintWriter pw = new PrintWriter(fw);
		for(String s : presenties){
			pw.println(s);
		}
		pw.close();
	}
	
	public void checkFile(String url) throws IOException{
		File f1 = new File(url);
		if (!f1.exists() || !f1.isFile()) {
			File f = new File(url);
			f.getParentFile().mkdirs(); 
			f.createNewFile();
		}
	}
	public ArrayList<Student> getStudentenVanLes(String gebruikersnaam){
		Docent docent = getDocent(gebruikersnaam);
		ArrayList<Les> lessen = docent.getLessen();
		String nu = LocalDate.now().toString();
		//.substring(0, LocalDate.now().toString().indexOf(" "));
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		Date now = null;
		try {
			now = df1.parse(nu);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("now: "+ now);
		DateFormat df = new SimpleDateFormat("HH:mm");
		String tijd = LocalTime.now().toString();
		tijd = tijd.substring(0, 5);
		System.out.println(tijd);
		Date current_time;
		try {
			current_time = df.parse(tijd);
			for(Les l : lessen){
				String temp_startTime = l.getStartTijd();
				String temp_endTime = l.getEindTijd();
				Date datum = l.getDatum();
				Date startTijd = df.parse(temp_startTime);
				Date eindTijd = df.parse(temp_endTime);					
				if(datum.compareTo(now) == 0 && startTijd.before(current_time) && eindTijd.after(current_time)){
					current = l;
				}
			}
			ArrayList<Student> studenten = getDeStudenten();
			ArrayList<Student> studentenVanLes = new ArrayList<Student>();
			for(Student s : studenten){
				ArrayList<Vak> mijnVakken = s.getMijnKlas().getMijnVakken();
				for(Vak v : mijnVakken){
					ArrayList<Les> mijnLessen = v.getLessen();
					for(Les les : mijnLessen){
					}
					
					if(mijnLessen.contains(current)){
						studentenVanLes.add(s);
					}
				}
			}
			
			
			return studentenVanLes;	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Les getCurrentLes(){
		return current;
	}
	public String login(String gebruikersnaam, String wachtwoord) {
		for (Docent d : deDocenten) {
			if (d.getGebruikersNaam().equals(gebruikersnaam)) {
				if (d.controleerWachtwoord(wachtwoord)) {
					return "docent";
				}
			}
		}
		
		for (Student s : deStudenten) {
			if (s.getGebruikersNaam().equals(gebruikersnaam)) {
				if (s.controleerWachtwoord(wachtwoord)) {
					System.out.println(s.getAbsentie());
					return "student";
				}
			}
		}
		
		return "undefined";
	}
	
	public Docent getDocent(String gebruikersnaam) {
		Docent resultaat = null;
		
		for (Docent d : deDocenten) {
			if (d.getGebruikersNaam().equals(gebruikersnaam)) {
				resultaat = d;
				break;
			}
		}
		
		return resultaat;
	}
	
	public ArrayList<Student> getDeStudenten(){
		return deStudenten;
	}
	
	public Student getStudent(String gebruikersnaam) {
		Student resultaat = null;
		
		for (Student s : deStudenten) {
			if (s.getGebruikersNaam().equals(gebruikersnaam)) {
				resultaat = s;
				break;
			}
		}
		
		return resultaat;
	}
	
	public ArrayList<Student> getStudentenVanKlas(String klasCode) {
		ArrayList<Student> resultaat = new ArrayList<Student>();
		
		for (Student s : deStudenten) {
			if (s.getMijnKlas().getKlasCode().equals(klasCode)) {
				resultaat.add(s);
			}
		}
		
		return resultaat;
	}
}

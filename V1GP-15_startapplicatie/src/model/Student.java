package model;

import java.util.ArrayList;
import java.util.Date;

public class Student extends Persoon{
	private int studentNummer;
	private String naam;
	private Klas mijnKlas;
	private ArrayList<Afwezigheid> absentie = new ArrayList<Afwezigheid>();
	private ArrayList<Presentie> presenties = new ArrayList<Presentie>();
	
	public Student(int studentNummer, String gebruikersNaam, String naam, String wachtwoord) {
		super(gebruikersNaam, wachtwoord);
		this.studentNummer = studentNummer;
		this.naam = naam;
	}
	
	public void setMijnKlas(Klas k) {
		mijnKlas = k;
	}
	
	public Klas getMijnKlas() {
		return mijnKlas;
	}
	
	public ArrayList<Afwezigheid> getAbsentie(){
		return this.absentie;
	}
	
	public boolean addAbsentie(Date startDatum){
		Afwezigheid x = new Afwezigheid(startDatum);
		int count = 0;
		for(Afwezigheid a : absentie){
			if(a.getEindDatum() == null){
				count++;
			}
		}
		if(count == 0){
			this.absentie.add(x);
			return true;
		}
		return false;
	}
	
	public boolean addAbsentie(Date startDatum, Date eindDatum, String reden){
		Afwezigheid x = new Afwezigheid(startDatum, eindDatum, reden);
		int count = 0;
		for(Afwezigheid a : absentie){
			if(a.toString().equals(x.toString())){
				count++;
			}
		}
		if(count == 0){
			this.absentie.add(x);
			return true;
		}else{
			return false;
		}
	}
	
	
	public void addPresentie(Les l, boolean aanwezig){
		Presentie p = new Presentie(l, aanwezig);
		presenties.add(p);
	}
	
	public Presentie getPresentie(Les l){
		for(Presentie p: presenties){
			if(p.getLes() == l){
				return p;
			}
		}
		return null;
	}
	
	public ArrayList<Presentie> getPresenties(){
		return presenties;
	}

	@Override
	public String toString() {
		return "Student [studentNummer=" + studentNummer + ", naam=" + naam + ", mijnKlas=" + mijnKlas + ", absentie="
				+ absentie + "]";
	}
}

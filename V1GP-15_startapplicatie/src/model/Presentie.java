package model;

public class Presentie {
	private boolean aanwezig;
	private Les les;
	
	public Presentie(Les l, boolean aanwezig){
		les = l;
		this.aanwezig = aanwezig;
	}
	
	public Les getLes(){
		return les;
	}
	
	public boolean getAanwezigheid(){
		return aanwezig;
	}
	
	public String toString(){
		String s = new String(les + "," + aanwezig);
		return s;
	}
}

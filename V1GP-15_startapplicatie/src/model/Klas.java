package model;

import java.util.ArrayList;

public class Klas {
	private ArrayList<Vak> mijnVakken = new ArrayList<Vak>();
	private String klasCode;
	
	public Klas(String kC) {
		klasCode = kC;
	}
	
	public String getKlasCode() {
		return klasCode;
	}
	
	public ArrayList<Vak> getMijnVakken(){
		return mijnVakken;
	}
	public void voegVakToe(Vak k){
		if(!mijnVakken.contains(k)){
			mijnVakken.add(k);
		}
					
	}
}

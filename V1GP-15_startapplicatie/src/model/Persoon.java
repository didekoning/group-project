package model;

import java.util.ArrayList;

public class Persoon {
	protected String gebruikersNaam;
	protected String wachtwoord;

	public Persoon(String  gebruikersNaam, String wachtwoord){
		this.gebruikersNaam = gebruikersNaam;
		this.wachtwoord = wachtwoord;
	}
	
	public boolean controleerWachtwoord(String ww) {
		return wachtwoord.equals(ww);
	}
	
	public String getGebruikersNaam() {
		return gebruikersNaam;
	}
}

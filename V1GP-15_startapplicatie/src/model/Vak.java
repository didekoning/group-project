package model;

import java.util.ArrayList;

public class Vak {


	private String vakCode;
	private String vakNaam;
	private ArrayList<Les> lessen = new ArrayList<Les>();
	
	public Vak(String vC, String vN) {
		vakCode = vC;
		vakNaam = vN;

	}
	
	public String getVakNaam() {
		return vakNaam;
	}
	
	public String getVakCode() {
		return vakCode;
	}
	
	public void addLes(Les les){
		this.lessen.add(les);
	}	
	
	public ArrayList<Les> getLessen(){
		return this.lessen;
	}
}
